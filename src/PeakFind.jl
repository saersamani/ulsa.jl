

function findpeaks(y,x,min_height,min_dist,min_prom = 0)

    dy = diff(y;dims =1)
    threshold = 0 

    peaks = in_threshold(dy, threshold)

    yP = y[peaks]
    peaks = with_prominence(y, peaks, min_prom)

#minimal height refinement
    peaks = peaks[y[peaks] .> min_height]
    yP = y[peaks]

    peaks = with_distance(peaks, x, y, min_dist)

    return peaks
end

"""
Select peaks that are inside threshold.
"""
function in_threshold(dy,threshold)

    peaks = 1:length(dy) |> collect

    k = 0
    for i = 2:length(dy)
        if dy[i] <= -threshold && dy[i-1] >= threshold
            k += 1
            peaks[k] = i
        end
    end
    peaks[1:k]
end

"""
Select peaks that have a given prominence
"""

function with_prominence(y,peaks,min_prom)

#minimal prominence refinement
    peaks[prominence(y, peaks) .> min_prom]
end


"""
Calculate peaks' prominences
"""

function prominence(y, peaks)
    yP = y[peaks]
    proms = zero(yP)

    for (i, p) in enumerate(peaks)
        lP, rP = 1, length(y)
        for j = (i-1):-1:1
            if yP[j] > yP[i]
                lP = peaks[j]
            break
        end
    end
    ml = minimum(y[lP:p])
    for j = (i+1):length(yP)
        if yP[j] > yP[i]
            rP = peaks[j]
            break
        end
    end
    mr = minimum(y[p:rP])
    ref = max(mr,ml)
    proms[i] = yP[i] - ref
    end

    proms
end

"""
Select only peaks that are further apart than `min_dist`
"""

function with_distance(peaks,x,y,min_dist)

    peaks2del = zeros(Bool, length(peaks))
    inds = sortperm(y[peaks], rev=true)
    permute!(peaks, inds)
    for i = 1:length(peaks)
        for j = 1:(i-1)
            if abs(x[peaks[i]] - x[peaks[j]]) <= min_dist
                if !peaks2del[j]
                    peaks2del[i] = true
                end
            end
        end
    end

    peaks[.!peaks2del]
end