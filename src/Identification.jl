using CSV
using DataFrames
using JLD
using FileIO
using Statistics
using LinearAlgebra
using HDF5
using ProgressBars
#using DecisionTree
using PyCall 
using ScikitLearn

# jl = pyimport("joblib")

@sk_import ensemble: RandomForestClassifier


###############################
# Write your package code here.
#
#
###############################
############################################################################
#
# Function to do id based on components batch mode

function featureID_comp_batch(mode,source,path2comps,weight_f)
    r = 0
    println("Databases are being loaded.")
    mm=pathof(ULSA)
    # mm = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src"
    # path2aux="/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/Database_2022-11-17.csv"
    path2aux=joinpath(mm[1:end-8],"Database_2022-11-17.csv")
    # path2aux = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/MassBankJulia.jld"
    # println(path2aux)
    path2aux2 = joinpath(mm[1:end-7],"CompTox.csv")
    # path2aux2 = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/CompTox.csv"

    path2aux3 = joinpath(mm[1:end-7],"ConfAssess_clf_model.joblib")
    # path2aux3 = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/ConfAssess_clf_model.joblib"
    # @load path2aux3 model

    clf = jl.load(path2aux3)

    #DB=load(path2aux,"MassBankJulia")
    DB = DataFrame(CSV.File(path2aux))

    comptox = CSV.File(path2aux2);
    println("Component list is being loaded.")

    # path2comps = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/test"
    files = readdir(path2comps)

    for i=1:size(files,1)

        m = split(files[i],".")

        if length(m) > 1 && m[end] == "csv" && m[1][end-4:end] == "_comp"
            path2spec = joinpath(path2comps,files[i])

            ID_comp(DB,comptox,mode,source,path2spec,weight_f,clf)
        end

    end

    r = 1

    return r 


end


############################################################################
#
# Function to do id based on components

function ID_comp(DB,comptox,mode,source,path2comp,weight_f,model)


    spec_list = DataFrame(CSV.File(path2comp))
    global final_table=zeros(1,20)

    # i=7
    for i in ProgressBar(1:size(spec_list,1))
        #println(i)
        id = spec_list[i,:].Nr
        rt = spec_list[i,:].Rt

        mass_tol = (spec_list[i,:].MaxMass-spec_list[i,:].MinMass)

        if spec_list.Parent[i]==1.0
            ms = spec_list[i,:].AccuMass
            min_mass = ms - mass_tol
            max_mass = ms + mass_tol

            spec = spec_list[i,:]
            ms2val,ms2int = comp_convert(spec)

            selected_lib = select_lib(DB,source,mode,min_mass,max_mass)
            #println(length(selected_lib))
            #println(keys(selected_lib))

            if length(selected_lib) > 0 && length(selected_lib["EXACT_MASS"]) > 0
                rep = entery_match(selected_lib,ms2val,ms2int,mass_tol,id,rt,ms,weight_f)



                if length(rep)>0
                    vect = pre_process(rep,sum(weight_f))
                    #p=apply_forest_proba(model, vect, ["0.0", "1.0"])
                    vect[isnan.(vect) .==1] .=0 
                    p = model.predict_proba(vect)
                    tv1=hcat(rep[:,1:13],p[:,2],rep[:,14:end])

                    global final_table=vcat(final_table,tv1)
                else
            
                    rep1=Array{Any}(undef,1,20)
                    rep1[1]=id
                    rep1[2]=rt
                    rep1[3]=ms
                    rep1[4]=NaN
                    rep1[5]=NaN
                    rep1[6]=NaN
                    rep1[7]=NaN
                    rep1[8]=NaN
                    rep1[9]=NaN
                    rep1[10]=NaN
                    rep1[11]=NaN
                    rep1[12]=NaN
                    rep1[13]=NaN
                    rep1[14]=NaN
                    rep1[15]=0
                    rep1[16]=NaN
                    rep1[17]=NaN
                    rep1[18]=NaN
                    rep1[19]=string(ms2val)
                    rep1[20]=string(ms2int)
                    global final_table=vcat(final_table,rep1)

                end
            else
                rep = mf_match(comptox,id,rt,ms,min_mass,max_mass)
                if length(rep)>0
                    tv1=hcat(rep[:,1:13],zeros(size(rep,1),1),rep[:,14:end])
                    global final_table=vcat(final_table,tv1)
                else

                    rep1=Array{Any}(undef,1,20)
                    rep1[1]=id
                    rep1[2]=rt
                    rep1[3]=ms
                    rep1[4]=NaN
                    rep1[5]=NaN
                    rep1[6]=NaN
                    rep1[7]=NaN
                    rep1[8]=NaN
                    rep1[9]=NaN
                    rep1[10]=NaN
                    rep1[11]=NaN
                    rep1[12]=NaN
                    rep1[13]=NaN
                    rep1[14]=NaN
                    rep1[15]=0
                    rep1[16]=NaN
                    rep1[17]=NaN
                    rep1[18]=NaN
                    rep1[19]= []
                    rep1[20]= []
                    global final_table=vcat(final_table,rep1)
                end
            end

        elseif spec_list.Parent[i]==0


            if mode == "POSITIVE"
                 ms=spec_list.MeasMass[i]- 1.007825
            elseif mode == "NEGATIVE"
                  ms=spec_list.MeasMass[i]- 1.007825
            end
            min_mass=ms - mass_tol/2
            max_mass=ms + mass_tol/2



            rep = mf_match(comptox,id,rt,ms,min_mass,max_mass)
            if length(rep)>0
                tv1=hcat(rep[:,1:13],zeros(size(rep,1),1),rep[:,14:end])
                global final_table=vcat(final_table,tv1)
            else
                
                rep1=Array{Any}(undef,1,20)
                    rep1[1]=id
                    rep1[2]=rt
                    rep1[3]=ms
                    rep1[4]=NaN
                    rep1[5]=NaN
                    rep1[6]=NaN
                    rep1[7]=NaN
                    rep1[8]=NaN
                    rep1[9]=NaN
                    rep1[10]=NaN
                    rep1[11]=NaN
                    rep1[12]=NaN
                    rep1[13]=NaN
                    rep1[14]=NaN
                    rep1[15]=0
                    rep1[16]=NaN
                    rep1[17]=NaN
                    rep1[18]=NaN
                    rep1[19]= []
                    rep1[20]= []
                global final_table=vcat(final_table,rep1)

            end


        end

    end

    table = DataFrame(final_table[2:end,:],[:ID,:Rt,:MS1Mass,:Name,:Formula,:ACCESSION,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:Probability,:FinalScore,
    :SpecType,:MatchedFrags,:Inchikey,:FragMZ,:FragInt])

    sort!(table,[:ID,:FinalScore],rev=[false, true])
    m=basename(path2comp)

    output=joinpath(dirname(path2comp),m[1:end-4]) * "_IDs.csv"
    CSV.write(output,table)

    return table

end # function


############################################################################
#
# A function to do id based on single component external

function featureID_ext(DB,mode,source,AccuMass,weight_f,mass_tol,ms2val,ms2int)



    min_mass=AccuMass - mass_tol
    max_mass=AccuMass + mass_tol

    mm=pathof(ULSA)

    #path2aux3 = joinpath(mm[1:end-7],"model_file.jld")
    path2aux3 = joinpath(mm[1:end-7],"ConfAssess_clf_model.joblib")

    #@load path2aux3 model

    clf = jl.load(path2aux3)

    selected_lib=select_lib(DB,source,mode,min_mass,max_mass)

    if length(selected_lib) > 0 && length(selected_lib["EXACT_MASS"]) > 0

        rep=entery_match(selected_lib,ms2val,ms2int,mass_tol,1,0,AccuMass,weight_f)
        vect = pre_process(rep,sum(weight_f))
        #p=apply_forest_proba(model, vect, ["0.0", "1.0"])
        vect[isnan.(vect) .==1] .=0 
        p = clf.predict_proba(vect)
        tv1=hcat(rep[:,1:13],p[:,2],rep[:,14:end])

        table=DataFrame(tv1,[:ID,:Rt,:MS1Mass,:Name,:Formula,:RefMatchFrag,:UsrMatchFrag,
        :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:FinalScore,:Probability,
        :SpecType,:MatchedFrags,:Inchikey,:FragMZ,:FragInt])

        sort!(table,[:ID,:FinalScore],rev=[false, true])
    else
        table=[]

    end


    return table

end # function


############################################################################
#
# Wrapping function to do id based on components

function featureID_comp(mode,source,path2comp,weight_f)

    println("Databases are being loaded.")
    mm=pathof(ULSA)
    # mm = "/Users/saersamanipour/.julia/packages/ULSA/XYtqP/src/ULSA.jl"
    path2aux=joinpath(mm[1:end-8],"Database_2022-11-17.csv")
    # path2aux = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/Database_2022-11-17.csv"
    # println(path2aux)
    path2aux2 = joinpath(mm[1:end-7],"CompTox.csv")
    # path2aux2 = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/CompTox.csv"

    #path2aux3 = joinpath(mm[1:end-7],"model_file.jld")
    path2aux3 = joinpath(mm[1:end-7],"ConfAssess_clf_model.joblib")
    # path2aux3 = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/ConfAssess_clf_model.joblib"

    #@load path2aux3 model

    clf = jl.load(path2aux3)


    #DB=load(path2aux,"MassBankJulia")

    DB = DataFrame(CSV.File(path2aux))  

    comptox = DataFrame(CSV.File(path2aux2));
    println("Component list is being loaded.")

    # path2comp = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/test/test_report_comp.csv"
    spec_list = DataFrame(CSV.File(path2comp))

    global final_table=zeros(1,20);

    # i=2
    for i in ProgressBar(1:size(spec_list,1))

        id=spec_list[i,:].Nr
        rt=spec_list[i,:].Rt

        mass_tol = (spec_list[i,:].MaxMass-spec_list[i,:].MinMass)

        if spec_list.Parent[i]==1.0
            ms=spec_list[i,:].AccuMass
            min_mass=ms - mass_tol
            max_mass=ms + mass_tol

            spec=spec_list[i,:]
            ms2val,ms2int = comp_convert(spec)

            selected_lib = select_lib(DB,source,mode,min_mass,max_mass)
            if isempty(selected_lib)
                continue
            end


            if length(selected_lib) > 0 && length(selected_lib["EXACT_MASS"]) > 0
                rep = entery_match(selected_lib,ms2val,ms2int,mass_tol,id,rt,ms,weight_f)
                #println(rep)
                if length(rep)>0
                    vect = pre_process(rep,sum(weight_f))
                     #p=apply_forest_proba(model, vect, ["0.0", "1.0"])
                    vect[isnan.(vect) .==1] .=0 
                    p = clf.predict_proba(vect)
                    tv1=hcat(rep[:,1:13],p[:,2],rep[:,14:end])
                    global final_table=vcat(final_table,tv1)
                else
                    
                    rep1=Array{Any}(undef,1,20)
                    rep1[1]=id
                    rep1[2]=rt
                    rep1[3]=ms
                    rep1[4]=NaN
                    rep1[5]=NaN
                    rep1[6]=NaN
                    rep1[7]=NaN
                    rep1[8]=NaN
                    rep1[9]=NaN
                    rep1[10]=NaN
                    rep1[11]=NaN
                    rep1[12]=NaN
                    rep1[13]=NaN
                    rep1[14]=NaN
                    rep1[15]=0
                    rep1[16]=NaN
                    rep1[17]=NaN
                    rep1[18]=NaN
                    rep1[19]=string(ms2val)
                    rep1[20]=string(ms2int)
                    global final_table=vcat(final_table,rep1)

                end
            else
                rep = mf_match(comptox,id,rt,ms,min_mass,max_mass)
                if length(rep)>0
                    tv1=hcat(rep[:,1:13],zeros(size(rep,1),1),rep[:,14:end])
                    global final_table=vcat(final_table,tv1)

                else

                    
                    rep1=Array{Any}(undef,1,20)
                    rep1[1]=id
                    rep1[2]=rt
                    rep1[3]=ms
                    rep1[4]=NaN
                    rep1[5]=NaN
                    rep1[6]=NaN
                    rep1[7]=NaN
                    rep1[8]=NaN
                    rep1[9]=NaN
                    rep1[10]=NaN
                    rep1[11]=NaN
                    rep1[12]=NaN
                    rep1[13]=NaN
                    rep1[14]=NaN
                    rep1[15]=0
                    rep1[16]=NaN
                    rep1[17]=NaN
                    rep1[18]=NaN
                    rep1[19]= []
                    rep1[20]= []
                    global final_table=vcat(final_table,rep1)
                end
            end

        elseif spec_list.Parent[i]==0


            if mode == "POSITIVE"
                 ms=spec_list.MeasMass[i]- 1.007825
            elseif mode == "NEGATIVE"
                  ms=spec_list.MeasMass[i]- 1.007825
            end
            min_mass=ms - mass_tol/2
            max_mass=ms + mass_tol/2



            rep = mf_match(comptox,id,rt,ms,min_mass,max_mass)
            if length(rep)>0
                tv1=hcat(rep[:,1:13],zeros(size(rep,1),1),rep[:,14:end])
                global final_table=vcat(final_table,tv1)
            else
                rep1=Array{Any}(undef,1,20)
                rep1[1]=id
                rep1[2]=rt
                rep1[3]=ms
                rep1[4]=NaN
                rep1[5]=NaN
                rep1[6]=NaN
                rep1[7]=NaN
                rep1[8]=NaN
                rep1[9]=NaN
                rep1[10]=NaN
                rep1[11]=NaN
                rep1[12]=NaN
                rep1[13]=NaN
                rep1[14]=NaN
                rep1[15]=0
                rep1[16]=NaN
                rep1[17]=NaN
                rep1[18]=NaN
                rep1[19] = []
                rep1[20] = []
                global final_table=vcat(final_table,rep1)

            end


        end

    end

    table = DataFrame(final_table[2:end,:],[:ID,:Rt,:MS1Mass,:Name,:Formula,:ACCESSION,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:Probability,:FinalScore,
    :SpecType,:MatchedFrags,:Inchikey,:FragMZ,:FragInt])

    sort!(table,[:ID,:FinalScore],rev=[false, true])
    m=basename(path2comp)

    output=joinpath(dirname(path2comp),m[1:end-4]) * "_IDs.csv"
    CSV.write(output,table)

    return table

end # function



############################################################################
#
# Wrapping function to do id based on components with extranl DB

function featureID_comp_ExDB(DB,mode,source,path2comp,weight_f)

    println("Databases are being loaded.")

    mm=pathof(ULSA)
    path2aux=joinpath(mm[1:end-7],"MassBankJulia.jld")
    # println(path2aux)
    path2aux2 = joinpath(mm[1:end-7],"CompTox.csv")

    #path2aux3 = joinpath(mm[1:end-7],"model_file.jld")
    path2aux3 = joinpath(mm[1:end-7],"ConfAssess_clf_model.joblib")

    #@load path2aux3 model

    clf = jl.load(path2aux3)


    #DB=load(path2aux,"MassBankJulia")


    #DB=load(path2aux,"MassBankJulia")

    comptox = DataFrame(CSV.File(path2aux2));
    println("Component list is being loaded.")

    spec_list = DataFrame(CSV.File(path2comp))

    global final_table=zeros(1,20);

    # i=7
    for i in ProgressBar(1:size(spec_list,1))

        id=spec_list[i,:].Nr
        rt=spec_list[i,:].Rt

        mass_tol=(spec_list[i,:].MaxMass-spec_list[i,:].MinMass)

        if spec_list.Parent[i]==1.0
            ms=spec_list[i,:].AccuMass
            min_mass=ms - mass_tol
            max_mass=ms + mass_tol

            spec=spec_list[i,:]
            ms2val,ms2int=comp_convert(spec)

            selected_lib=select_lib(DB,source,mode,min_mass,max_mass)



            if length(selected_lib) > 0 && length(selected_lib["EXACT_MASS"]) > 0
                rep = entery_match(selected_lib,ms2val,ms2int,mass_tol,id,rt,ms,weight_f)
                # println(i)
                if length(rep)>0
                    vect = pre_process(rep,sum(weight_f))
                     #p=apply_forest_proba(model, vect, ["0.0", "1.0"])
                    vect[isnan.(vect) .==1] .=0  
                    p = clf.predict_proba(vect)
                    tv1=hcat(rep[:,1:13],p[:,2],rep[:,14:end])
                    global final_table=vcat(final_table,tv1)
                else
                    rep1=Array{Any}(undef,1,20)
                    rep1[1]=id
                    rep1[2]=rt
                    rep1[3]=ms
                    rep1[4]=NaN
                    rep1[5]=NaN
                    rep1[6]=NaN
                    rep1[7]=NaN
                    rep1[8]=NaN
                    rep1[9]=NaN
                    rep1[10]=NaN
                    rep1[11]=NaN
                    rep1[12]=NaN
                    rep1[13]=NaN
                    rep1[14]=NaN
                    rep1[15]=0
                    rep1[16]=NaN
                    rep1[17]=NaN
                    rep1[18]=NaN
                    rep1[19]=string(ms2val)
                    rep1[20]=string(ms2int)
                    global final_table=vcat(final_table,rep1)

                end
            else
                rep = mf_match(comptox,id,rt,ms,min_mass,max_mass)
                if length(rep)>0
                    tv1=hcat(rep[:,1:13],zeros(size(rep,1),1),rep[:,14:end])
                    global final_table=vcat(final_table,tv1)

                else

                    rep1=Array{Any}(undef,1,20)
                    rep1[1]=id
                    rep1[2]=rt
                    rep1[3]=ms
                    rep1[4]=NaN
                    rep1[5]=NaN
                    rep1[6]=NaN
                    rep1[7]=NaN
                    rep1[8]=NaN
                    rep1[9]=NaN
                    rep1[10]=NaN
                    rep1[11]=NaN
                    rep1[12]=NaN
                    rep1[13]=NaN
                    rep1[14]=NaN
                    rep1[15]=0
                    rep1[16]=NaN
                    rep1[17]=NaN
                    rep1[18]=NaN
                    rep1[19]=string(ms2val)
                    rep1[20]=string(ms2int)
                    global final_table=vcat(final_table,rep1)
                end
            end

        elseif spec_list.Parent[i]==0


            if mode == "POSITIVE"
                 ms=spec_list.MeasMass[i]- 1.007825
            elseif mode == "NEGATIVE"
                  ms=spec_list.MeasMass[i]- 1.007825
            end
            min_mass=ms - mass_tol/2
            max_mass=ms + mass_tol/2



            rep = mf_match(comptox,id,rt,ms,min_mass,max_mass)
            if length(rep)>0
                tv1=hcat(rep[:,1:13],zeros(size(rep,1),1),rep[:,14:end])
                global final_table=vcat(final_table,tv1)
            else
                rep1=Array{Any}(undef,1,20)
                rep1[1]=id
                rep1[2]=rt
                rep1[3]=ms
                rep1[4]=NaN
                rep1[5]=NaN
                rep1[6]=NaN
                rep1[7]=NaN
                rep1[8]=NaN
                rep1[9]=NaN
                rep1[10]=NaN
                rep1[11]=NaN
                rep1[12]=NaN
                rep1[13]=NaN
                rep1[14]=NaN
                rep1[15]=0
                rep1[16]=NaN
                rep1[17]=NaN
                rep1[18]=NaN
                rep1[19]=string(ms2val)
                rep1[20]=string(ms2int)
                global final_table=vcat(final_table,rep1)

            end


        end

    end

    table = DataFrame(final_table[2:end,:],[:ID,:Rt,:MS1Mass,:Name,:Formula,:ACCESSION,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:Probability,:FinalScore,
    :SpecType,:MatchedFrags,:Inchikey,:FragMZ,:FragInt])

    sort!(table,[:ID,:FinalScore],rev=[false, true])
    m=basename(path2comp)

    output=joinpath(dirname(path2comp),m[1:end-4]) * "_IDs.csv"
    CSV.write(output,table)

    return table

end # function


############################################################################
# Convert the MS1 and MS2 spectra in one vector

 function comp_convert(spec)

    if spec.MS1Comp != "0" && spec.MS1Comp != "Float64[]" && spec.MS1Comp != 0 && spec.MS1Comp != "[]"

        ms1_val=parse.(Float64,split(spec.MS1Comp[2:end-1],", "))
        ms1_int=parse.(Float64,split(spec.MS1CompInt[2:end-1],", "))
    else
        ms1_val=0
        ms1_int=0

    end

    if spec.MS2Comp != "0" && spec.MS2Comp != "Float64[]" && spec.MS2Comp != 0 && spec.MS2Comp != "[]"

        ms2_val=parse.(Float64,split(spec.MS2Comp[2:end-1],", "))
        ms2_int=parse.(Float64,split(spec.MS2CompInt[2:end-1],", "))
    else
        ms2_val=0
        ms2_int=0
    end

    ms2val=vcat(ms1_val,ms2_val)
    ms2val=ms2val[ms2val .>0]
    ms2int=vcat(ms1_int,ms2_int)
    ms2int=ms2int[ms2int .>0]

    return(ms2val,ms2int)


end


####################################
# selecting the library entries

function select_lib(DB,source,mode,min_mass,max_mass)


    imass = findall(x-> min_mass < x < max_mass, DB[!,"EXACT_MASS"])
    
    if isempty(imass)
        return []
    end

    #selected_lib=Dict()
    ind=zeros(1,length(imass))
    # i=2
    for i=1:length(imass)
        if length(DB[!,"IONIZATION"][imass[i]]) >0 && DB[!,"IONIZATION"][imass[i]] == source &&
            length(DB[!,"ION_MODE"][imass[i]]) >0 && DB[!,"ION_MODE"][imass[i]] == mode
            ind[i]=imass[i]
        end

    end

    filtered_lib=Dict("INCHIKEY"=>DB[!,"INCHIKEY"][Int.(ind[ind .>0])],"NAME"=>DB[!,"NAME"][Int.(ind[ind .>0])]
    ,"MZ_INT"=>DB[!,"MZ_INT"][Int.(ind[ind .>0])],"EXACT_MASS"=>DB[!,"EXACT_MASS"][Int.(ind[ind .>0])]
    ,"DATA_TYPE"=>DB[!,"DATA_TYPE"][Int.(ind[ind .>0])],"MZ_VALUES"=>DB[!,"MZ_VALUES"][Int.(ind[ind .>0])]
    ,"FORMULA"=>DB[!,"FORMULA"][Int.(ind[ind .>0])],"ACCESSION"=>DB[!,"ACCESSION"][Int.(ind[ind .>0])])

    return filtered_lib


end # function

###########################################################################
# Parsing database enteries 

function parse_db_entry(entry)
    if length(entry) >5
        n = split(entry[5:end-1],",")
        e_parsed = parse.(Float64,n)

    end 

    return e_parsed
end




###########################################################################
# Fragment entreies


function entery_match(selected_lib,ms2val,ms2int,mass_tol,id,rt,ms,weight_f)

    n_ref=size(selected_lib["EXACT_MASS"],1)

    rep=Array{Any}(undef,n_ref,19)
    # i=1

    for i=1:n_ref

        #mz_ref=if selected_lib["MZ_VALUES"][i] != "NA" ;copy(selected_lib["MZ_VALUES"][i]); else []; end
        #int_ref=if selected_lib["MZ_INT"][i] != "NA";copy(selected_lib["MZ_INT"][i]); else []; end
        mz_ref = parse_db_entry(selected_lib["MZ_VALUES"][i])
        int_ref = parse_db_entry(selected_lib["MZ_INT"][i])
        mz_user=copy(ms2val)
        int_user=copy(ms2int)
        ref_mzs,n_ref_match,ref_match_e_av,ref_match_e_std = frag_match(mz_ref[mz_ref .> 0],mz_user,mass_tol)

        if n_ref_match>0
            master_vect_f = master_mass_gen(mz_ref,int_ref,mz_user,int_user,mass_tol)
            direct_m,revers_m = dot_prod(master_vect_f)
        else
            direct_m=0
            revers_m=0
        end

        ms1_e=selected_lib["EXACT_MASS"][i]-ms
        scors= score_calc(mz_ref,mz_user,n_ref_match,ref_match_e_av,
            ref_match_e_std,ms1_e,mass_tol,weight_f)
        final_score=round(sum(scors)+weight_f[6]*direct_m+weight_f[7]*revers_m,digits=2)
        rep[i,1]=id
        rep[i,2]=rt
        rep[i,3]=ms
        rep[i,4]=selected_lib["NAME"][i]
        rep[i,5]=selected_lib["FORMULA"][i]
        rep[i,6]=selected_lib["ACCESSION"][i]
        rep[i,7]=string(n_ref_match)*"--"*string(length(mz_ref))
        rep[i,8]=string(n_ref_match)*"--"*string(length(mz_user))
        rep[i,9]=round(ms1_e,digits=3)
        rep[i,10]=round(ref_match_e_av,digits=3)
        rep[i,11]=round(ref_match_e_std,digits=3)
        rep[i,12]=direct_m
        rep[i,13]=revers_m
        rep[i,14]=final_score
        rep[i,15]=selected_lib["DATA_TYPE"][i]
        rep[i,16]=string(ref_mzs)
        rep[i,17]=selected_lib["INCHIKEY"][i]
        rep[i,18]=string(ms2val)
        rep[i,19]=string(ms2int)


    end



    return(rep)

end



############################################################################
# Fragment matching
#

function frag_match(mz_ref,mz_user,mass_tol)

    ref_match=zeros(length(mz_ref))
    ref_match_e=zeros(length(mz_ref))

    mz_user_1=copy(mz_user)



    for i=1:length(mz_ref)
        tv1=abs.(mz_ref[i] .- mz_user_1 )
        tv2=findall(x -> x <= mass_tol/2, tv1)
        if length(tv2[tv2 .> 0]) > 0
            ref_match[i]=1
            mz_user_1[tv2] .=0
            ref_match_e[i] = mean(tv1[tv2])
        end
    end



    ref_mzs=mz_ref[ref_match .> 0]
    n_ref_match=sum(ref_match)
    ref_match_e_av=mean(ref_match_e)
    ref_match_e_std=std(ref_match_e)


    return(ref_mzs,n_ref_match,ref_match_e_av,ref_match_e_std)

end



############################################################################
# master mass list generator
#

function master_mass_gen(mz_ref,int_ref,mz_user,int_user,mass_tol)

    mz_values=vcat(mz_ref[:],mz_user[:])
    master_vect=zeros(length(vcat(mz_ref[:],mz_user[:])),3)

    for i=1:size(master_vect,1)
        tv1=abs.(mz_values[i] .- mz_values)
        tv2=findall(x -> x <= mass_tol/2, tv1)
        master_vect[i,1]=mean(mz_values[tv2])
        mz_values[tv2].=0
    end

    for i=1:size(master_vect,1)
        if master_vect[i] > 0
            #println(i)
            tv1=abs.(master_vect[i] .- mz_ref)
            tv2=findall(x -> x <= mass_tol/2, tv1)
            if length(tv2)>0
                master_vect[i,2]=maximum(int_ref[tv2])
                int_ref[tv2] .=0
            end

            ttv1=abs.(master_vect[i] .- mz_user)
            ttv2=findall(x -> x <= mass_tol/2, ttv1)
            if  length(ttv2) >0
                if maximum(ttv2) <= length(int_user)
                    master_vect[i,3]=maximum(int_user[ttv2])
                    int_user[ttv2] .=0
                elseif maximum(ttv2) > length(int_user)

                    tv3 = zeros(size(int_user))
                    for j = 1:length(ttv2)
                        if ttv2[j] > length(int_user)
                            ttv2[j]=0
                        end

                    end
                    master_vect[i,3]=maximum(int_user[ttv2[ttv2 .>0]])
                    int_user[ttv2[ttv2 .>0]] .=0
                end
            end
        end


    end


    master_vect_f=master_vect[master_vect[:,1] .> 0,:]

    return master_vect_f
end





############################################################################
# Dot product calculations
#

function dot_prod(master_vect_f)

    norm_ref_spec=(master_vect_f[:,1] .* sqrt.(master_vect_f[:,2])) ./ sum(master_vect_f[:,1] .* sqrt.(master_vect_f[:,2]))
    norm_user_spec=(master_vect_f[:,1] .* sqrt.(master_vect_f[:,3])) ./ sum(master_vect_f[:,1] .* sqrt.(master_vect_f[:,3]))

    dot_ref=dot(norm_ref_spec,norm_ref_spec)
    dot_user=dot(norm_user_spec,norm_user_spec)

    dot_direct=dot(norm_user_spec,norm_ref_spec)
    if dot_direct>0
        direct_m=round(1-abs(dot_ref-dot_direct),digits=2)
        revers_m=round(1-abs(dot_user-dot_direct),digits=2)
    else
        direct_m=0
        revers_m=0
    end

    return(direct_m,revers_m)
end


############################################################################
# Matching score calculator
#

function score_calc(mz_ref,mz_user,n_ref_match,ref_match_e_av,
    ref_match_e_std,ms1_e,mass_tol,weight_f)

    if n_ref_match > 0
        s_match_frag_ref=round(weight_f[1]*(n_ref_match/length(mz_ref)),digits=2)
        s_match_frag_usr=round(weight_f[2]*(n_ref_match/length(mz_user)),digits=2)

        s_ms2_e=abs(weight_f[4]*round((abs(ref_match_e_av)-(0.5*mass_tol))/(0.5*mass_tol),digits=2))
        s_ms2_std=abs(weight_f[5]*round((abs(ref_match_e_std)-(0.5*mass_tol))/(0.5*mass_tol),digits=2))
        if isnan(s_ms2_std)==1
            s_ms2_std=0
        end
    else
        s_match_frag_ref=0
        s_match_frag_usr=0
        s_ms2_e=0
        s_ms2_std=0
    end

    s_ms1_e=abs(weight_f[3]*round((abs(ms1_e)-(mass_tol))/(mass_tol),digits=2))


    return([s_match_frag_ref,s_match_frag_usr,s_ms1_e,s_ms2_e,s_ms2_std])
end

############################################################################
# Molecular formula match

function mf_match(comptox,id,rt,ms,min_mass,max_mass)


    ind_sel = findall(x -> min_mass < x < max_mass, comptox.MONOISOTOPIC_MASS_DTXCID)
    ind_u = indexin(unique(comptox.FORMULA_INDIVIDUAL_COMPONENT[ind_sel]), comptox.FORMULA_INDIVIDUAL_COMPONENT[ind_sel])
    ms1e= ms .- comptox.MONOISOTOPIC_MASS_DTXCID[ind_sel[ind_u]]

    n_ref=length(ind_u)

    rep=Array{Any}(undef,n_ref,19);
    # i=1
    rep[:,1] .=id
    rep[:,2] .=rt
    rep[:,3] .=ms
    rep[:,4] .= NaN
    rep[:,5] = comptox.FORMULA_INDIVIDUAL_COMPONENT[ind_sel[ind_u]]
    rep[:,6] .=NaN
    rep[:,7] .=NaN
    rep[:,8] .=NaN
    rep[:,9] =round.(ms1e,digits=3)
    rep[:,10] .=NaN
    rep[:,11] .=NaN
    rep[:,12] .=NaN
    rep[:,13] .=NaN
    rep[:,14] =if length(ms1e) > 0;round.(1 .- abs.(ms1e ./ maximum(abs.(ms1e))),digits=2);else [];end
    rep[:,15] .="EXP"
    rep[:,16] .=NaN
    rep[:,17] .=NaN
    rep[:,18] .=NaN
    rep[:,19] .=NaN



    return(rep)
end # function



############################################################################
#
# Wrapping function to do molecular formula based on components and EPA-CompTox

function featureMF_comp(mode,source,path2comp)


    mm=pathof(ULSA)
    path2aux2 = joinpath(mm[1:end-7],"CompTox.csv")
    comptox = DataFrame(CSV.File(path2aux2));
    spec_list = DataFrame(CSV.File(path2comp));
    global final_table=zeros(1,21);

    # i=2
    for i in ProgressBar(1:size(spec_list,1))

        id=spec_list[i,:].Nr
        rt=spec_list[i,:].Rt

        mass_tol=(spec_list[i,:].MaxMass-spec_list[i,:].MinMass)

        if mode == "POSITIVE" && source == "ESI"
             ms=spec_list.MeasMass[i]- 1.007825
        elseif mode == "NEGATIVE" && source == "ESI"
              ms=spec_list.MeasMass[i]- 1.007825
        end
        min_mass=ms - mass_tol/2
        max_mass=ms + mass_tol/2

        #println(i)

        rep = mf_match(comptox,id,rt,ms,min_mass,max_mass)
        if length(rep)>0
            rep1=Array{Any}(undef,size(rep,1),21)
            rep1[:,1] .=id
            rep1[:,2] .=spec_list.ScanNum[i]
            rep1[:,3] .=spec_list.ScanInPeak[i]
            rep1[:,4] .=spec_list.Rt[i]
            rep1[:,5] .=spec_list.MinInPeak[i]
            rep1[:,6] .=spec_list.MeasMass[i]
            rep1[:,7] .=spec_list.MinMass[i]
            rep1[:,8] .=spec_list.MaxMass[i]
            rep1[:,9] .=spec_list.Area[i]
            rep1[:,10] .=spec_list.Int[i]
            rep1[:,11] .=spec_list.FeatPurity[i]
            rep1[:,12] .=spec_list.MediRes[i]
            rep1[:,13] .=spec_list.Parent[i]
            rep1[:,14] .=spec_list.AccuMass[i]
            rep1[:,15] =rep[:,5]
            rep1[:,16] =rep[:,8]
            rep1[:,17] =round.(rep[:,13],digits=3)
            rep1[:,18] .=spec_list.MS1Comp[i]
            rep1[:,19] .=spec_list.MS1CompInt[i]
            rep1[:,20] .=spec_list.MS2Comp[i]
            rep1[:,21] .=spec_list.MS2CompInt[i]

            global final_table=vcat(final_table,rep1)
        else
            rep1=Array{Any}(undef,1,21)
            rep1[1] =id
            rep1[2] =spec_list.ScanNum[i]
            rep1[3] =spec_list.ScanInPeak[i]
            rep1[4] =spec_list.Rt[i]
            rep1[5] =spec_list.MinInPeak[i]
            rep1[6] =spec_list.MeasMass[i]
            rep1[7] =spec_list.MinMass[i]
            rep1[8] =spec_list.MaxMass[i]
            rep1[9] =spec_list.Area[i]
            rep1[10] =spec_list.Int[i]
            rep1[11] =spec_list.FeatPurity[i]
            rep1[12] =spec_list.MediRes[i]
            rep1[13] =spec_list.Parent[i]
            rep1[14] =spec_list.AccuMass[i]
            rep1[15] =NaN
            rep1[16] =NaN
            rep1[17] =NaN
            rep1[18] =spec_list.MS1Comp[i]
            rep1[19] =spec_list.MS1CompInt[i]
            rep1[20] =spec_list.MS2Comp[i]
            rep1[21] =spec_list.MS2CompInt[i]
            global final_table=vcat(final_table,rep1)

        end




    end

    table = DataFrame(final_table[2:end,:],[:ID,:ScanNum,:ScanInPeak,:Rt,:MinInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,
    :MolecFormula,:MassError,:Score,:MS1Comp,:MS1CompInt,:MS2Comp,:MS2CompInt])

    sort!(table,[:ID,:Score],rev=[false, true])
    m=basename(path2comp)

    output=joinpath(dirname(path2comp),m[1:end-4]) * "_MFs.csv"
    CSV.write(output,table)

    return table

end

#####################################################################
# Function to pre-process the entries for RF filtering


function pre_process(entry,tot_score)


    vect=zeros(size(entry,1),12);

    for i=1:size(entry,1)

        f_r=parse.(Float64,split(entry[i,7],"--"))
        vect[i,1]=f_r[1]
        vect[i,2]=f_r[2]
        vect[i,3]=round(100*f_r[1]/f_r[2])

        f_u=parse.(Float64,split(entry[i,8],"--"))
        vect[i,4]=f_u[1]
        vect[i,5]=f_u[2]
        vect[i,6]=round(100*f_u[1]/f_u[2])

        vect[i,7]=entry[i,9]
        vect[i,8]=entry[i,10]
        vect[i,9]=entry[i,11]
        vect[i,10]=entry[i,12]
        vect[i,11]=entry[i,13]
        vect[i,12]=round(100*entry[i,14]/tot_score)


    end



    return vect

end

# "ACCESSION"

#################################################
# test area 
"""

path_d = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/Database_2022-11-17.csv"

mode = "POSITIVE"
source = "ESI"
path2comp = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/test/test_report_comp.csv"

weight_f =[1,1,1,1,1,1,1]

# featureID_comp(mode,source,path2comp,weight_f)

"""