__precompile__()
module ULSA

using Pkg
using CSV
using DataFrames
using JLD
using FileIO
using Statistics
using LinearAlgebra
using HDF5
using ProgressBars
#using DecisionTree
using ScikitLearn
using BenchmarkTools

using Conda
using Downloads

if Sys.iswindows()
    #move miniconda installer in case it is in the Conda/3/ folder
    pathConda = pathof(Conda)
    if contains(pathConda, "\\")
        pathConda = split(pathConda, "\\packages")[1]
    # else
    #     pathConda = split(pathConda, "/packages")[1]
    end
    pathConda = joinpath(pathConda, "conda", "3","installer.exe")



    if isfile(pathConda)
        mv(pathConda, pathConda[1:end-15]*pathConda[end-12:end], force = true)
    else 
        Downloads.download("https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe",pathConda)
#        cp(pathConda, pathConda[1:end-15]*pathConda[end-12:end], force = true)
    end
end

using PyCall

Conda.add("joblib")

const jl = PyNULL()

function __init__()
    copy!(jl,pyimport("joblib"))

end


include("Identification.jl")
include("PeakFind.jl")
include("Screening.jl")
include("Screening_list_generator.jl")

export featureMF_comp, featureID_comp, featureID_comp_batch, featureID_ext, featureID_comp_ExDB, suslist2entries, proteine_screening_ms1, suspect_screening, screening_swath, screening_dia, 
       screening_mc, screening_dda, screening_ms1, constructSuspectlistSeperate, constructSuspectlistMerged

###############################


#######################################################


end # end of the module


#################################################
# test area 
 """

mode = "POSITIVE"
source = "ESI"
path2comp = "/Users/saersamanipour/Downloads/20180308-pos-S11D6-EFF-A_report_comp.csv"

weight_f =[1,1,1,1,1,1,1]

# featureID_comp(mode,source,path2comp,weight_f)

"""