# ULSA.jl

[![Build Status](https://ci.appveyor.com/api/projects/status/github/saersamani/ULSA.jl?svg=true)](https://ci.appveyor.com/project/saersamani/ULSA-jl)



 **ULSA.jl** is a julia package for the identification of the components (i.e. features including the MS1 and MS2 signal) based on [MassBank](https://massbank.eu/MassBank/). The **ULSA.jl** performs library search and generate a list of candidates for each component. For cases where the identification is impossible, the algorithm assigns a molecular formula to the features based on the [US-EPA CompTox](https://comptox.epa.gov/dashboard) database. The algorithm is explained in details [elsewhere](https://pubs.acs.org/doi/abs/10.1021/acs.est.8b00259).

 Please cite when using **ULSA.jl** for you own work.
 Samanipour et al. *Environ. Sci. Technol.* 2018, 52, 8, 4694–4701.

## Installation

Given that **ULSA.jl** is not a registered julia package, for installation the *url* to the repository and the julia package manager are necessary.

```julia
using Pkg
Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/ulsa.jl/src/master/"))


```

## Usage
There are three main functions implemented via **ULSA.jl**, namely: *featureID_comp(--)*, *featureID_comp_batch(--)* and *featureMF_comp(--)*. These functions enable the automated generation of candidate lists based on the component lists created by [**CompCreate.jl**]("https://bitbucket.org/SSamanipour/compcreate.jl/src/master/").

###  featureID_comp(--)
This function performs the identification of the component list and saves the report as a CSV file in the same folder as the component list.
```julia
using ULSA
using JLD # This package is necessary for loading the Auxiliary file
using CSV # This package is necessary for loading the Auxiliary file


IDs = featureID_comp(mode::String,source::String,path2comp::String,weight_f::Array{Any})


```
#### Inputs featureID_comp(--)
* **mode::String** is the acquisition mode/polarity. It must be either "POSITIVE" or "NEGATIVE".
* **source::String** is the acquisition source. At the moment the algorithm only has been tested for "ESI".
* **path2comp::String** is the path to the csv file containing the components.
* **weight_f::Array{Any}** is an array of seven values ranging between 0 and 1. Each value defines the weight given to each search parameter.
#### Output featureID_comp(--)

This function generates a DataFrame with the candidates as well as a CSV file with the candidates saved in the folder of the component list. An example of such file is available in the [test folder](https://bitbucket.org/SSamanipour/ulsa.jl/src/master/test/test_report_comp_IDs.csv).

###  featureID_comp_batch(--)
This function performs the identification of the component list in a batch mode and saves the report as a CSV file in the same folder as the component list.
```julia
using ULSA
using JLD # This package is necessary for loading the Auxiliary file
using CSV # This package is necessary for loading the Auxiliary file


IDs = featureID_comp_batch(mode::String,source::String,path2comps::String,weight_f::Array{Any})


```
#### Inputs featureID_comp_batch(--)
* **mode::String** is the acquisition mode/polarity. It must be either "POSITIVE" or "NEGATIVE".
* **source::String** is the acquisition source. At the moment the algorithm only has been tested for "ESI".
* **path2comps::String** is the path to the folder with csv file containing the components.
* **weight_f::Array{Any}** is an array of seven values ranging between 0 and 1. Each value defines the weight given to each search parameter.
#### Output featureID_comp_batch(--)

This function generates DataFrames with the candidates as well as a CSV files with the candidates saved in the folder of the component lists. An example of such file is available in the [test folder](https://bitbucket.org/SSamanipour/ulsa.jl/src/master/test/test_report_comp_IDs.csv).


### featureMF_comp(--)
This function performs the molecular formula assignment employing the [US-EPA CompTox](https://comptox.epa.gov/dashboard) database. The final output is saved in a report as a CSV file in the same folder as the component list.

```julia
using ULSA
using JLD # This package is necessary for loading the Auxiliary file
using CSV # This package is necessary for loading the Auxiliary file


MFs = featureMF_comp(mode::String,source::String,path2comp::String)

```


### Examples

For more examples on how to process your own data please take a look at folder [examples](https://bitbucket.org/SSamanipour/ulsa.jl/src/master/examples/).


## Contribution

Issues and pull requests are welcome! Please contact the developers for further information.
