using ULSA
using Pkg
using Test
using CSV
using DataFrames
using JLD
#using DecisionTree



try 
    using MS_Import
    #using Cent2Profile
catch
    @warn("MS_Import is being installed")
    Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/ms_import.jl/src/master/"))
    #Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/cent2profile.jl/src/master/"))
    using MS_Import
    #using Cent2Profile
end

#include("/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/ULSA.jl")

#"""
@testset "Dependencies" begin

    mm=pathof(ULSA)
    path2comp="test_report_comp.csv"
    spec_list = DataFrame(CSV.File(path2comp));


    @test isfile(joinpath(mm[1:end-7],"ConfAssess_clf_model.joblib")) == 1
    @test isfile(joinpath(mm[1:end-7],"CompTox.csv")) == 1
    @test spec_list.Parent[1] == 0

end


@testset "ULSA.jl" begin

    path2comp="test_report_comp.csv"
    weight_f=[1,1,1,1,1,1,1]
    mode="POSITIVE"
    source="ESI"


    table = featureID_comp(mode,source,path2comp,weight_f)
    table1 = featureMF_comp(mode,source,path2comp)
    r = featureID_comp_batch(mode,source,pwd(),weight_f)

    @test size(table,1) == 128
    @test table.MS1Mass[1] == 83.952275
    #@test table1.Score[3] == 0
    @test r == 1

end

#"""

@testset "SuspScreen.jl" begin

    path2sus = "Test_susList.csv"
    mode = "POSITIVE"
    source = "ESI"
    

    # File import
    pathin=""
    filenames=["TestChrom.mzXML"]
    mz_thresh = [100,400]
    int_thresh = 500  
    
    chrom = import_files(pathin,filenames,mz_thresh,int_thresh)

    #print(chrom)


    # Converting the enrties 

    sus_e = suslist2entries(path2sus);
    #println(sus_e)

    # Suspect Screening 
    mass_tol = 0.05
    min_int = 2000
    iso_depth = 5
    rt_width = 0.5

    table = suspect_screening(chrom,sus_e,mass_tol,min_int,rt_width,iso_depth)
    println(table)
    @test table[!,"NrMatchedFrags"][1] == 4
    @test table[!,"NrTestedFrags"][1] == 35
    @test table[!,"MatchFactor"][3] == 0.66


end